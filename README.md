# DWManga

Bash script that search and download manga and also support Tor/proxy through proxychains and curl

## Downloading

Clone GitHub Repository

     $ git clone https://gitlab.com/TanyaEleventhGoddess/dwmanga && cd dwmanga

## Disclaimer

Please note that **this is an alpha version**, so **it's expected that something will not work as intended or will cease to work.**
